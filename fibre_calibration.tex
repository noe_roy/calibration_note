The first step of the calibration was the channel equalization. It consisted in three main tasks: MPPC gain and pedestal equalization, individual fibre attenuation length computation, fibre to fibre inhomogeneities correction (mostly coming from optical coupling between the fibres and the MPPCs).

\subsection{Gain and pedestal measurement}
A gain equalization was made at the beginning of the SoLid Phase 1 data taking. All the gains were set to 31.5 [ADC/PA]. Due to external condition variation (temperature for instance) variations of a few percent were expected during the two years of data taking. 
A reference pedestal per channel was used fir the data acquisition. This value was not updated frequently and did not take into account a possible drift in time. 
To correct those gain and pedestal evolution effects, the two parameters were computed in a 6h frequency to take into account a possible day/night evolution. A small sample of the data were processed every run and used for the data quality monitoring. In those data, the amplitude distributions per channels were gathered in 6 hour time range. In this distribution the TSpectrum ROOT object combined with Gaussian fits was used to find the amplitude peak positions and to get the amplitude as a function of the number of photo avalanches (PA) (Fig. \ref{fig:gain}). The parameters could then be derived using equation \ref{eq:gain}:

\begin{equation}
\mathrm{Amplitude~[PA]} = \mathrm{Amplitude~[ADC]} \times  \mathrm{Gain ~[ADC/PA]} +  \mathrm{Drift~[PA]}
\label{eq:gain}
\end{equation}

The pedestal drift was then added the reference pedestal to get working value per channel in a 6h time range. The gain and pedestal time evolution of ten channels is represented on Fig. \ref{fig:time_chan_evolution}. The jumps observed are coming from period where data taking were stopped due to technicals issues (chiller issues, pressure test, maintenance...) or calibrations. The peaks after the jumps are not fully understood but the main suspect is the temperature variation after the physics data taking restarting due to the temperature equalization response time. The variations of the gain were within a few percent during the whole calibration period and the pedestal variations were bellow one percent assuring that the MPPCs response was stable over time. Systematic studies were performed to assess the robustness of the fitting procedure, the error for both the gain and the pedestal measurement were bellow 1$\%$.
%
\begin{figure}[]
\centering
\includegraphics[width=\textwidth]{img/gain.pdf}
\caption{{\small{Amplitude spectrum for a given channel (left). Fit of the Amplitude as a function of the PA peak number (right). The slope is the gain and the intercept is the pedestal drift.}}}
\label{fig:gain}
\end{figure}



%
\begin{figure}[]
	\centering
	\includegraphics[width=\textwidth]{img/Gain_all_period_10_chans.pdf}
	\includegraphics[width=\textwidth]{img/Pedestal_all_period_10_chans.pdf}
	\caption{{\small{MMPC gain (top) and pedestal (bottom) variation during Phase 1. }}}
	\label{fig:time_chan_evolution}
\end{figure}


\subsection{Fibre attenuation}
\label{sec:attenuation}
Once the MPPCs response were equalized the individual fibre attenuation length were measured using $^{22}$Na calibration data. After a rough calibration with the procedure described in \ref{sec:ccalib} and only gain and pedestal correction, cubes with 1 MeV deposits were selected. For a given fibre, the fraction of light recorded by the MPPC as a function of the cube distance to the MPPC was measured. The light fraction should follow a double exponential shape as described in these equations: 

\begin{equation}
\begin{aligned}
&f_{bottom} = A \times \left( e^{-\frac{x\times L_{cube}+L_{cube}/2+L_0}{ \lambda}} + R_{mirror}\times e^{-\frac{(2L_{fibre}-(x\times L_{cube}+L_{cube}/2+L_0))}{ \lambda}} \right) \\
&f_{noEbox} = A \times \left( e^{-\frac{y\times L_{cube}+L_{cube}/2+L_0}{ \lambda}} + R_{mirror}\times e^{-\frac{(2L_{fibre}-(y\times L_{cube}+L_{cube}/2+L_0))}{ \lambda}} \right) \\
&f_{top} =A \times \left( e^{-\frac{(15 - x)\times L_{cube}+L_{cube}/2+L_0}{ \lambda}} + R_{mirror}\times e^{-\frac{(2L_{fibre}-((15 - x)\times L_{cube}+L_{cube}/2+L_0))}{ \lambda}} \right) \\
&f_{Ebox} = A \times \left( e^{-\frac{(15 - y)\times L_{cube}+L_{cube}/2+L_0}{ \lambda}} + R_{mirror}\times e^{-\frac{(2L_{fibre}-((15 - y)\times L_{cube}+L_{cube}/2+L_0))}{ \lambda}} \right) \\
\end{aligned}
\label{eq:attenuation}
\end{equation}

where :
\begin{conditions}
	f_{side} & Attenuation model used for the MPPC placed in a given side \\
	A & Arbitrary amplitude \\
	x,y & Cube position in SoLid's coordinate system \\
	\lambda & Attenuation length \\
	R_{mirror} & Mirror reflectivity (0.8) \\
	L_{cube} & Cube size (5.08 cm) \\
	L_0 & Distance between the last cube and the MPPC (5.27 cm) \\
	L_{fibre} & The size of the fibre (92.24 cm)
\end{conditions}

Half of the light going directly to the MPPC and the other half being reflected first by a mirror at the other end of the fibre. The coefficient $R_{mirror}$ was put arbitrarily at 0.8. The same value was used in the simulation of the attenuation effect in order to have a equivalent model in simulation and reconstruction. The attenuation pattern was then fitted for every channel (see Fig. \ref{fig:Attenuation}). The specific attenuation pattern for every channel are represented in the Plane book \cite{planebook}. The average value measured for the attenuation length is 950 mm for a spread of around 12$\%$ (see Fig. \ref{fig:attenuation_distribution}). Only 3128 channels out of 3200 were measured due to 36 dead channels: only cubes with 4 active channels were considered so 72 channels were left aside. 

%
\begin{figure}[]
\centering
\includegraphics[width=.49\textwidth]{img/attenuation_plane.pdf}
\includegraphics[width=.49\textwidth]{img/attenuation_pattern2.pdf}
\caption{{\small{Light fraction seen by the fibre with the MPPC in the bottom side of a plane (left). Attenuation pattern obtained for one of the fibre (right).}}}
\label{fig:Attenuation}
\end{figure}


\begin{figure}[]
	\centering
	\includegraphics[width=.77\textwidth]{img/attenuation_distrib.pdf}
	\caption{\small{Attenuation distribution for the active channels }}
	\label{fig:attenuation_distribution}
\end{figure}


\subsection{Optical fibre - MPPC coupling}
After attenuation correction, the last channel related parameter to quantify was the optical coupling between the fibres and the MPPCs. In a given plane, for a given side of the plane, the light measured by each MPPC was compared to the maximum light recorded by all of them (see Fig. \ref{fig:Coupling}). There might be other effects to take into account such as differences of mirror reflectivity, coupling with the mirrors or between the fibres and the cubes. But all those effects were gathered in this parameter quantifying the channel to channel inhomogeneities. A distribution of those coupling parameters is shown in Fig. \ref{fig:coupling_distribution} showing a spread of around 1$\%$ between the channels.

%
\begin{figure}[h!]
\centering
\includegraphics[width=.41\textwidth]{img/couplng_plane.pdf}
\includegraphics[width=.41\textwidth]{img/couplng_pattern.pdf}
\caption{{\small{Light seen by the fibre with the MPPC in the bottom side of a plane after attenuation correction (in PA) (left). Coupling parameter for all the MPPCs on the bottom sides (right).}}}
\label{fig:Coupling}
\end{figure}




\begin{figure}[h!]
	\centering
	\includegraphics[width=.77\textwidth]{img/coupling_distrib.pdf}
	\caption{\small{Coupling parameter distribution for the measured channels.}}
	\label{fig:coupling_distribution}
\end{figure}


\subsection{Alternative method}
Due to correlations between the different parameters in the plane, another method was developed to measure the attenuation and coupling parameters of the channels of a plane in a combined fit. This method is based on the asymmetries between the 4 fibres in a cube. Defining 3 types of asymmetry (horizontal - vertical , left - right, top - bottom) as in Eq. \ref{eq:asymetry}. 
\begin{equation}
  \begin{aligned}
    A_{H-V} &= \frac{E_{L}+E_{R} - (E_{T}+E_{B})}{E_{L}+E_{R}+E_{T}+E_{B}}\\
    A_{L-R} &= \frac{E_{L}-E_{R}}{E_{L}+E_{R}}\\
    A_{T-B} &= \frac{E_{T}-E_{B}}{E_{T}+E_{B}}\\
    E_{T,B,L,R} &= E_{dep}\cdot LY \cdot (\epsilon^{att,dir}_{T,B,L,R}+\epsilon^{att,ref}_{T,B,L,R})\cdot \epsilon^{coup}_{T,B,L,R}
  \end{aligned}
  \label{eq:asymetry}
\end{equation}

where:
\begin{description}
\item[$A_{H-V}$] is the horizontal - vertical asymmetry
\item[$A_{L-R}$] is the left - right asymmetry
\item[$A_{T-B}$] is the Top - Bottom asymmetry
\item[$E_{i}$] is the light fraction seen by the fibre in side $i$
\item[$\epsilon^{att,dir (ref)}_{i}$] is the attenuation factor for the direct (reflected) light for the fibre in side $i$
\item[$\epsilon^{coup}_{i}$] is the coupling factor for the fibre in side $i$
\end{description}

Each asymmetry being a function of both the fibre attenuation and coupling, the 128 parameters per plane could directly be measured in a global fit. The precedent methods are used to initialize the fitter due to the large number of parameters.
To chose the most accurate method, a comparison between the fitted and the expected values have been performed on $^{22}Na$ simulations as shown on Fig. \ref{fig:comp_methods}. With the asymmetry method the bias in the measurement of the attenuation lengths was slightly better than the other method (1.7$\%$ compared to 2.4) with values in a smaller range ($\sigma = $4.2$ \%$ compared to 6.9). The measurement on the the coupling gives roughly roughly the same values, whatever method considered. Due to the improvement on the parameter measurement, the asymmetry method has been chosen as the default method for SoLid calibration.


\begin{figure}[h!]
	\centering
	\includegraphics[width=0.49\textwidth]{img/attenuation_methods.pdf}
	\includegraphics[width=0.49\textwidth]{img/coupling_methods.pdf}
	\caption{\small{ Comparison input vs measurement for attenuation length (left) and coupling (right).}}
	\label{fig:comp_methods}
\end{figure}
