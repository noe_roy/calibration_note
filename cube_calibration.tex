\subsection{Calibration methods}
\label{sec:methods}
Due to the small size of the detection cells (5x5x5 \cubic{\centi\meter}) and the use of light elements (PVT) the probability of a photon leaving all it's energy in only one cube is small. After the Compton interaction, the gamma will usually not deposit enough energy to be detected. Due to this specificity of the detector, Compton Edges were used as energy calibration reference point. Two methods have been developed for this purpose: an analytical fit, based on Klein Nishina formula and a data/Monte-Carlo comparison based method.

\subsubsection{Analytical fit}

In the cases where the gamma will leave a fraction of it's energy in a cube, leaving the cube after the interaction without any more energy deposit, the cross section of the interaction can be derived by the Klein Nishina formula:
\begin{equation}{\label{KN}}
\frac{d\sigma}{dT}=\frac{\pi r_e^{2}}{m_ec^2\alpha^2}\left (2+\left (\frac{T}{E_0-T}\right )^2 \left (\frac{1}{\alpha^2}+\frac{E_0-T}{E_0}-\frac{2}{\alpha}\left(\frac{E_0-T}{T}\right ) \right ) \right )
\end{equation}
where $r_e$ is the classical radius of the electron, $m_e$ is the electron's mass, $\alpha$ is the fine structure constant, $E_0$ is the initial energy of the gamma and T is the electron energy after scattering.
The cross section being non null for $T \in [0,E_c]$ with $E_c$ the Compton edge energy:
\begin{equation}
E_c = E_0 \left(1 - \frac{1}{1+\frac{2E_0}{m_ec^2}}\right)
\end{equation}
A numerical convolution is then applied to the model to get an energy spectrum pdf the following way:

\begin{equation}
	f_{conv}(x) = \sum_{i=0}^{i_{E_c}} \frac{d\sigma}{dT}(T_i) \frac{1}{\sqrt{2\pi}\sigma_0\sqrt{T_i}} e^{-0.5\frac{(\frac{x}{LY} - T_i)^2}{\sigma_0^2 T_i}}
\end{equation}
Where x is the amplitude in PA, and where the two parameters to fit are the light yield, $LY$ and $\sigma_0$, the energy resolution. This p.d.f has then to be normalized:

\begin{equation}
f_{conv}(x) = \frac{\sum_{i=0}^{i_{E_c}} \frac{d\sigma}{dT}(T_i) \frac{1}{\sqrt{2\pi}\sigma_0\sqrt{T_i}} e^{-0.5\frac{(\frac{x}{LY} - T_i)^2}{\sigma_0^2 T_i}}}{\sum_{i=0}^{i_{E_c}} \frac{d\sigma}{dT}(T_i)}
\end{equation}

Some correction due to reconstruction inefficiencies and energy loss of the gammas between the source and the cube of interaction had then to be taken into account. The efficiency correction was computed with the ratio between the number of generated events at the \GEANTfour level and the number events that were reconstructed and selected given a set of calibration cuts as shown in Fig. \ref{fig:efficiency}. The energy loss due to the gamma interactions is computed with the ratio of energy deposit between a cube in front of the source and the considered cube, again using \GEANTfour true energies. One can then arrive to the fit of a cube as represented in Fig. \ref{fig:analytical_fit}.
\begin{figure}[h!]
	\centering
	\includegraphics[width=.7\textwidth]{img/ratio_efficiency.pdf}
	\caption{{Efficiency plot where the ratio of events that pass the reconstruction and the selection cuts is computed, fitted and stored to create calibration templates.}}
	\label{fig:efficiency}
\end{figure}


\begin{figure}[h!]
	\centering
	\includegraphics[width=.7\textwidth]{img/CE_analytical.pdf}
	\caption{{Analytical fit for a given cube.}}
	\label{fig:analytical_fit}
\end{figure}


\subsubsection{Geant 4 - Data comparison}\label{sec:ks}
The second method was based on a comparison between calibration data and Monte-Carlo simulations. For a given cube, the true energy deposits from \GEANTfour-based Monte-Carlo simulation were numerically convoluted with a given energy resolution and a reconstruction efficiency correction was applied the same way as the previous method. On Fig. \ref{fig:template} is shown the effect of the modification of the G4 spectrum.




\begin{figure}[h!]
	\centering
	\includegraphics[width=.49\textwidth]{img/g4_bis.pdf}
	\includegraphics[width=.49\textwidth]{img/g4_eres.pdf}
	\caption{{\small True \GEANTfour energy spectrum (left), convoluted spectrum (right). The energy distributions are numerically convoluted to create the calibration templates.}}
	\label{fig:template}
\end{figure}



A simulation template was created that way for each cube and each energy resolution. The energy deposits in the data were scaled with a given scaling factor, light yield in PA/MeV. A scan of scaling factors and energy resolutions was made with a test of agreement between the scaled data and the convoluted simulation was performed (Chi square or Kolmogorov-Smirnov test). The couple that gave the best agreement was chosen. Errors on the parameters could be derived from a $\Delta\chi^2$ analysis or the K-S test profile as represented in Fig. \ref{fig:maps_tests}. To do so for instance with the light yield, the results of the statistical test were projected on the light yield space, selecting for a given energy resolution the light yield that gave the best score as shown in Fig. \ref{fig:paraboles}. The error here could be estimated at 2$\%$ with a $\Delta\chi^2$ at one $\sigma$.

\begin{figure}[h!]
	\centering
	\includegraphics[width=.49\textwidth]{img/map_kolmo.pdf}
	\includegraphics[width=.49\textwidth]{img/map_chi2.pdf}
	\caption{{\small Kolmogorov Smirnov test (left) results or $\Delta\chi^2$ (right) for the given couples (Eres,light yield) for a given cube. The best couple will be the one that maximises the Kolmogorov-Smirnov test or minimizes the $\chi^2$ test.}}
	\label{fig:maps_tests}
\end{figure}

\begin{figure}[h!]
	\centering
	\includegraphics[width=.49\textwidth]{img/ks_parabole.pdf}
	\includegraphics[width=.49\textwidth]{img/chi2_parabole.pdf}
	\caption{{\small Kolmogorov Smirnov test result (left) results or $\Delta\chi^2$ (right) As a function of the light yield. Each point corresponds to the light yield that maximises the likelihood for a given energy resolution.}}
	\label{fig:paraboles}
\end{figure}


\subsection{Calibration performance}
After the channel level calibration, the visible light yield of each cube was measured. This specific calibration was done using the two methods described in section \ref{sec:methods} on cubes with 4 active fibres with a difference in average within 2$\%$ between the two methods. From this measurement can be derived a 2D light yield map of the detector as shown in Fig. \ref{fig:maply} for the calibration of September 2018. For the version of the reconstruction that was used for this calibration, the attenuation effects presented in Section \ref{sec:attenuation} were not corrected. Those effects will be taken into account in the next version of the reconstruction algorithm. The attenuation effect can be seen with cubes in the middle of the plane having a smaller visible light yield than the one one on the sides.
\subsubsection{Light Yield homogeneity}
After a measurement of the individual visible cube light yields, the homogeneity of the detector have been measured with a spread of 7$\%$ of light yield as shown on the Fig. \ref{fig:maply} right, with an averaged light yield of 96.3 PA/MeV. The light yield variation plane per plane is represented on Fig. \ref{fig:planes_ly} left, a 17$\%$ variation can be seen at the largest between two planes but most of them are within 5$\%$. On the Fig. \ref{fig:modules_ly} the light yield per module is represented where the difference in the average ly is at most at 5$\%$.  

\begin{figure}[h!]
	\centering
 	\includegraphics[width=.49\textwidth]{img/LY_Map.pdf}
	\includegraphics[width=.49\textwidth]{img/LY_1D.pdf}
	\caption{{\small{Light yield by cube positions, averaged in z position (left). Light yield distribution on the cubes with 4 active channels (right).}}}
	\label{fig:maply}
\end{figure}


\begin{figure}[h!]
	\centering
	\includegraphics[width=\textwidth]{img/plane_ly.pdf}
	\caption{{\small{Light yield variation per plane}}}
	\label{fig:planes_ly}
\end{figure}


\begin{figure}[h!]
	\centering
	\includegraphics[width=\textwidth]{img/module_ly.pdf}
	\caption{{\small{Light yield variation per module}}}
	\label{fig:modules_ly}
\end{figure}


\subsubsection{Energy response linearity}
The linearity of the detector was tested using May-June 2020 calibration campaign. For this study, only the statistical method presented in section \ref{sec:ks} was used. 5 calibration points were used between 0.5 and 4.2 MeV using  $^{207}$Bi, $^{22}$Na calibrations runs with the \emph{periodic trigger} and AmBe with the \emph{threshold trigger}. Due to time limitation and to optimize the statistics for each calibration point, the sources were put on only 2 positions: in the middle and in the top corner, electronic box side, of Gap 5. A total of 239 cubes, $\simeq$ 2 \% of the detector, were tested. For the different calibration sources, different selection criteria were applied to measure the Compton edges:
\begin{itemize}
	\item[--] For the $^{22}$Na and $^{207}$Bi, an isolation criterium was applied. Only events where no other cubes in the plane of interest were selected in order to remove the contamination from light leakages of neighbouring cubes or pile up effects in the fibres. An effect of the isolation selection is shown in Fig. \ref{fig:isolation}.\\
	\item[--]  For the AmBe, due to the high energy of the Compton edge, there was systematically light leakages from the cube of interest to neighbouring cubes which removed the isolation option. Due to the simultaneous emission of a neutron and a gamma by the source, a spatial and time coincidence was required between the two signals.  
\end{itemize}

\begin{figure}[h!]
	\centering
	\includegraphics[width=\textwidth]{img/isolation.pdf}
	\caption{\small{Energy distribution for $^{207}$Bi 0.6 MeV gamma with and without the isolation selection.}}
	\label{fig:isolation}
\end{figure}


Each cube light yield was measured for the different gammas. Those values were then averaged per calibration gamma to get an averaged linearity measurement on the tested cubes as shown in Fig. \ref{fig:linearity}. The light yield obtained was also fitted for all cubes and the slope distribution is represented on the same figure. We achieve a spread of about 5\% which is what we expect from a single calibration point measurement for those cubes. 

\begin{figure}[h!]
	\centering
	\includegraphics[width=.49\textwidth]{img/Final_linearity_kolmo.pdf}
	\includegraphics[width=.49\textwidth]{img/slope_distrib.pdf}
	\caption{{\small{Linearity measured on Gap5 with 2 sources position(left). Slope of the individual cube linearity distribution (right).}}} 
	\label{fig:linearity}
\end{figure}

\subsubsection{Time evolution}
The light yield has been measured for several calibration campaigns, and the average light yield evolution through time is represented on Fig. \ref{fig:evolution_light_yield}. Two periods can be isolated: before and after May 2019 where the average light yield dropped of a few percent. This drops happened just after a heating of the detector of around $50 \degree$C due to a chiller failure and an issue in the monitoring procedure during a maintenance of the detector. It is believed that this heating caused a degradation of the cube light yield. The average decreasing of light yield through time seems also to have raised from $-1.2 \%$ to $-3.3 \%$ per year.
\begin{figure}[h!]
	\centering
	\includegraphics[width=.9\textwidth]{img/time_evolution.pdf}
	\caption{{\small{evolution of light yield in the detector. The evolution seen is around 3.6 $\%$ per year. An effect of the heating of the detector can be seen around March 2019.}}} 
	\label{fig:evolution_light_yield}
\end{figure}

\subsection{Energy resolution}
The energy resolution of a given cube in our detector can be described by the general quadratic sum:

\begin{equation}
\frac{\sigma}{E} =  \frac{a}{E} \oplus \frac{b}{\sqrt{E}}\oplus c
\label{eq:resolution}
\end{equation}

where :
\begin{itemize}
	\item[-] a is a noise contribution induced by electronic noise in the readout.\\
	\item[-] b is a stochastic contribution due to statistical fluctuations on the number of avalanches induced by a given energy deposit.\\
	\item[-] c is a constant contribution independent of the energy deposit that comes from light leakages and dead materials in the detector.	
\end{itemize}

In the same way as the linearity, a measurement of the cube energy resolution was performed via the method described in \ref{sec:ks}. In average the constant, stochastic and noise parameters measured were 12$\%$, 11$\%$ and 3$\%$ as shown in \ref{fig:eres}. 

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.7\textwidth]{img/eres01.pdf}
	\caption{\small{Energy resolution measurement for 4 different calibration points.}}
	\label{fig:eres}
\end{figure}


However in this, the calibration methods are under the assumption that we only have a stochastic term in the energy resolution (either on the analytical fit or the numerical convolution). Indeed the convolution applied each time is following a resolution of $b \times \sqrt{E}$. At some extent this could induce an error in the measurements if the Compton Edge is fitted on a too large range. Furthermore, in SoLid, the energy estimators have clustered information combining several cubes. One idea (not presented here) to quantify this energy resolution would be to make a Data - Monte Carlo comparison, studying for a given known energy deposit the width of the reconstructed energy peak with a given energy estimator. Fitting those with the model presented in equation \ref{eq:resolution} would give us the information on the resolution expected in SoLid.








